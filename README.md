# React PoolChat

Deploy do PoolChat - <https://paloochat-frontend.herokuapp.com/>

## POOLCHAT INTERFACE DE ENTRADA

![POOLCHAT INTERFACE DE ENTRADA](./client/image/join.png)

## Cliente e Servidor

## POOLCHAT ÁREA DE CONVERSASÃO

![POOLCHAT ÁREA DE CONVERSASÃO](./client/image/chat.png)

A pasta do cliente está utilizando create-react-ap e socket.io-client. Foi feito o processo de componetização dos elementos para melhor disposição. Foi-se implementado em html, js e css. Foi feito o deploy do mesmo atraves do Heroku para que todos consigam ter o acesso.

O servidor é alimentado por Nodejs e Express. O aplicativo de chat está sendo executado no Socket.io, para melhor performace e otimização das mensagens de envio e resposta. Além disso, também foi feito o deploy do servidor na plataforma Heroku.


## Equipe

* Kleydson Beckman
* João Pedro Gomes
* Michelle Soraya
